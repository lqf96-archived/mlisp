module MLispExe.REPL where

import Data.Map as Map (Map, fromList, member, (!))
import Data.Maybe (fromMaybe)
import Data.ByteString as BS (ByteString, pack, empty, length, append)
import System.IO (BufferMode(..), hSetBuffering, hFlush, stdout, stdin)
import System.Exit (exitSuccess)
import Control.Monad.Trans (lift)
import Control.Monad.State (StateT, get, modify, runStateT)

import MLisp.VM (
    MLispVM(..),
    MLispScope,
    MLispOp,
    makeVM,
    runVM,
    runOp,
    printObj,
    getReg,
    runIO)
import MLisp.Parser (Statement(..), parseProgram)
import MLisp.Compiler (MLispTransOp, evalTransOp, makeTransCtx, transformAST, genByteCode, getCode, setOffset)
import MLispExe.Code (translate, handleVMError, legalizeChars)

import Debug.Trace

-- [ Constants ]
-- Command-to-handler mapping
cmdHandlerMapping :: Map String (String -> REPLOp ())
cmdHandlerMapping = fromList [
        ("help", printHelp),
        ("i", runProgram),
        ("t", printAST),
        ("q", exitREPL),
        ("|", runMultiLineProgram)
    ]

-- [ Types ]
-- REPL state
data MLispREPLState = MLispREPLState {
    -- Previous program source code
    prevSource :: Maybe String,
    -- All bytecode
    allByteCode :: ByteString,
    -- Code length
    byteCodeLen :: Int,
    -- Global scope
    globalScope :: Maybe MLispScope
}

-- REPL operation
type REPLOp = StateT MLispREPLState IO

-- [ Functions ]
-- Get global variables
getGlobals :: REPLOp (Maybe MLispScope)
getGlobals = fmap globalScope get

-- Set global variables
setGlobals :: MLispScope -> REPLOp ()
setGlobals globals = modify (\state -> state { globalScope = Just globals })

-- Get source
getSource :: REPLOp (Maybe String)
getSource = fmap prevSource get

-- Set source
setSource :: String -> REPLOp ()
setSource newSource = modify (\state -> state { prevSource = Just newSource })

-- Get code length
getCodeLen :: REPLOp Int
getCodeLen = fmap byteCodeLen get

-- Set code length
setCodeLen :: Int -> REPLOp ()
setCodeLen newOffset = modify (\state -> state { byteCodeLen = newOffset })

-- Get all byte code
getAllByteCode :: REPLOp ByteString
getAllByteCode = fmap allByteCode get

-- Set all byte code
setAllByteCode :: ByteString -> REPLOp ()
setAllByteCode newByteCode = modify (\state -> state { allByteCode = newByteCode })

-- Incremental translation
incrementalTranslate :: Int -> Statement -> MLispTransOp (ByteString, Int)
incrementalTranslate offset ast = setOffset offset >> translate ast False

-- REPL virtual machine operations
replVMOp :: Statement -> MLispOp ()
replVMOp ast = do
    -- Run VM
    runVM
    -- Print value for expression
    case ast of
        (StmExpr _) -> do
            exprVal <- getReg 0
            printObj exprVal
            runIO $ putStrLn ""
        _ -> return ()

-- Run given program
runProgram :: String -> REPLOp ()
runProgram rawInput = do
    let input = fmap legalizeChars rawInput
    -- Set previous source
    maybeSourceCode <- if input == ""
        then getSource
        else setSource input >> return (Just input)
    case maybeSourceCode of
        -- No previous source code
        Nothing -> lift $ putStrLn "Error: No previous source code."
        (Just sourceCode) -> do
            -- Get previous bytecode length
            prevOffset <- getCodeLen
            -- Parse program
            case parseProgram sourceCode of
                (Left err) -> lift $ putStrLn ("Error: Parse error: " ++ err)
                -- Transform AST and get bytecode
                (Right ast) -> case evalTransOp (incrementalTranslate prevOffset ast) makeTransCtx of
                    (Left err) -> lift $ putStrLn ("Error: AST transform error: " ++ show err)
                    (Right (byteCode, entryPoint)) -> do
                        -- Get global scope and all bytecode
                        scope <- getGlobals
                        prevByteCode <- getAllByteCode
                        let newByteCode = prevByteCode `append` byteCode
                        -- Build VM state
                        let vm = makeVM {
                            globals = fromMaybe (globals makeVM) scope,
                            program = newByteCode,
                            counter = entryPoint
                        }
                        -- Run VM and print value for expression
                        (result, newVM) <- lift $ runOp (replVMOp ast) vm
                        -- Prompt runtime error
                        case result of
                            (Left err) -> lift $ handleVMError err newVM putStr
                            _ -> return ()
                        -- Set global scope, all bytecode and new offset
                        setGlobals $ globals newVM
                        setCodeLen $ BS.length newByteCode
                        setAllByteCode newByteCode

-- Multi-line reader
multiLineReader :: IO String
multiLineReader = do
    -- Prompt
    putStr "MLisp| "
    hFlush stdout
    -- Read line
    line <- getLine
    if line == ":|"
        -- End of multi-line input
        then return []
        -- Accumulate input
        else do
            inputs <- multiLineReader
            return (line ++ "\n" ++ inputs)

-- Run multi-line program
runMultiLineProgram :: String -> REPLOp ()
runMultiLineProgram _ = lift multiLineReader >>= runProgram

-- Print AST (of last command)
printAST :: String -> REPLOp ()
printAST rawInput = do
    let input = fmap legalizeChars rawInput
    -- Get source code
    maybeSourceCode <- if input == ""
        then getSource
        else return $ Just input
    case maybeSourceCode of
        -- No previous source code
        Nothing -> lift $ putStrLn "Error: No previous source code."
        (Just sourceCode) -> do
            -- Parse program
            case parseProgram sourceCode of
                (Left err) -> lift $ putStrLn ("Error: Parse error: " ++ err)
                -- Transform and show AST
                (Right ast) -> lift $ print ast

-- Print help
printHelp :: String -> REPLOp ()
printHelp _ = lift $ do
    putStrLn "[ MLisp REPL Help ]"
    putStrLn ":help             Print this help."
    putStrLn "[:i] [<program>]  Run given / last program."
    putStrLn "[:|] <program> :| Run multi-line program."
    putStrLn ":t [<program>]    Print AST (of last command)."
    putStrLn ":q                Exit the program."

-- Exit program
exitREPL :: String -> REPLOp ()
exitREPL _ = lift exitSuccess

-- Parse input
parseInput :: String -> (String, String)
-- No input (Special situation)
parseInput "" = ("", "")
-- Input with command given
parseInput (':':input) =
    let (cmd, remain) = span (/= ' ') input
        (_, arg) = span (== ' ') remain
    in (cmd, arg)
-- Defaults to ":i" command
parseInput input = parseInput (":i " ++ input)

-- REPL loop
replLoop :: REPLOp ()
replLoop = do
    -- Get input
    input <- lift $ do
        -- Prompt
        putStr "MLisp> "
        hFlush stdout
        -- Get line
        getLine
    -- Parse input; do nothing with no input
    if input == ""
        then return ()
        else do
            let (cmd, arg) = parseInput input
            -- Call command if exists; otherwise prompt error
            if member cmd cmdHandlerMapping
                then (cmdHandlerMapping ! cmd) arg
                else lift $ putStrLn ("Error: No such command \"" ++ cmd ++ "\".")
    -- Infinite recurse
    replLoop

-- Run REPL
runREPL :: IO ()
runREPL = do
    -- Use line buffering
    hSetBuffering stdin LineBuffering
    hSetBuffering stdout LineBuffering
    -- Welcome message
    putStrLn "MLisp 0.1.0 REPL. Type ':help' for help."
    -- Call REPL loop function
    runStateT replLoop MLispREPLState {
        prevSource = Nothing,
        allByteCode = BS.empty,
        byteCodeLen = 0,
        globalScope = Nothing
    }
    return ()
