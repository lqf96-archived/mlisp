{-# LANGUAGE OverloadedStrings #-}

module MLispTest.Parser.TestParser where

import Test.QuickCheck
import MLispTest.TestCase
import MLisp.Parser
import Data.Attoparsec.Text
import Data.Either

testParserEntry :: IO()
testParserEntry = do 
    quickCheck testStmListParserR
    quickCheck testStmListParserW1
    quickCheck testStmListParserW2
    quickCheck testStmListParserW3
    putStrLn "stmListParser finished"
    putStrLn ""
    putStrLn ""
    quickCheck testSkipParserR1
    quickCheck testSkipParserR2
    quickCheck testSkipParserW1
    quickCheck testSkipParserW2
    quickCheck testSkipParserW3
    putStrLn "-----skipParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testIfStmParserR
    quickCheck testIfStmParserW
    putStrLn "-----ifStmParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testWhileStmParserR1
    quickCheck testWhileStmParserR2
    quickCheck testWhileStmParserW1
    quickCheck testWhileStmParserW2
    quickCheck testWhileStmParserW3
    putStrLn "-----whileStmParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testReturnStmParserR1
    quickCheck testReturnStmParserR2
    quickCheck testReturnStmParserR3
    quickCheck testReturnStmParserW1
    quickCheck testReturnStmParserW2
    quickCheck testReturnStmParserW3
    quickCheck testReturnStmParserW4
    putStrLn "-----returnStmParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testSetVarExprR1
    quickCheck testSetVarExprR2
    quickCheck testSetVarExprR3
    quickCheck testSetVarExprW1
    quickCheck testSetVarExprW2
    putStrLn "-----setVarParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testMakeVecExprR
    quickCheck testMakeVecExprW
    putStrLn "-----makeVecParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testVecSetExprR1
    quickCheck testVecSetExprR2
    quickCheck testVecSetExprW1
    quickCheck testVecSetExprW2
    quickCheck testVecSetExprW3
    quickCheck testVecSetExprW4
    putStrLn "-----vecSetParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testStmExprParserR
    putStrLn "-----stmParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testStatementListParserR
    quickCheck testStatementListParserW
    putStrLn "-----statementListParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testNumberLiteralExprR
    quickCheck testNumberLiteralExprW1
    quickCheck testNumberLiteralExprW2
    quickCheck testNumberLiteralExprW3
    putStrLn "-----numberLiteralParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testCharLiteralExprR
    quickCheck testCharLiteralExprW1
    quickCheck testCharLiteralExprW2
    quickCheck testCharLiteralExprW3
    quickCheck testCharLiteralExprW4
    putStrLn "-----charLiteralParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testStringLiteralExprR
    quickCheck testStringLiteralExprW1
    quickCheck testStringLiteralExprW2
    quickCheck testStringLiteralExprW3
    putStrLn "-----stringLiteralParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testBoolLiteralExprR1
    quickCheck testBoolLiteralExprR2
    quickCheck testBoolLiteralExprW1
    quickCheck testBoolLiteralExprW2
    quickCheck testBoolLiteralExprW3
    quickCheck testBoolLiteralExprW4
    putStrLn "-----boolLiteralParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testNilLiteralExprR1
    quickCheck testNilLiteralExprR2
    quickCheck testNilLiteralExprR3
    quickCheck testNilLiteralExprW1
    quickCheck testNilLiteralExprW2
    putStrLn "-----nilLiteralParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testVariableValueExprR
    putStrLn "-----variableValueParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testFunctionCallExprR1
    quickCheck testFunctionCallExprR2
    quickCheck testFunctionCallExprW1
    quickCheck testFunctionCallExprW2
    quickCheck testFunctionCallExprW3
    putStrLn "-----functionCallParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testLambdaDefExprR
    quickCheck testLambdaDefExprW1
    quickCheck testLambdaDefExprW2
    quickCheck testLambdaDefExprW3
    quickCheck testLambdaDefExprW4
    putStrLn "-----lambdaDefParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testFunctionDefStmR1
    quickCheck testFunctionDefStmR1
    quickCheck testFunctionDefStmW1
    quickCheck testFunctionDefStmW2
    quickCheck testFunctionDefStmW3
    quickCheck testFunctionDefStmW4
    putStrLn "-----functionDefParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testLexeme
    putStrLn "-----lexeme finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testIdentifierParserR1
    quickCheck testIdentifierParserR2
    quickCheck testIdentifierParserW1
    quickCheck testIdentifierParserW2
    putStrLn "-----identifierParser finished-----"
    putStrLn ""
    putStrLn ""
    quickCheck testInvalidCharR1
    quickCheck testInvalidCharR2
    quickCheck testInvalidCharR3
    quickCheck testInvalidCharR4
    quickCheck testInvalidCharR5
    quickCheck testInvalidCharW1
    quickCheck testInvalidCharW2
    putStrLn "-----invalidCharParser finished-----"


--stmListParser
--right statement
testStmListParserR = (parseOnly stmListParser "(begin skip 1 2)" == Right expectedStmList)
--wrong statement
testStmListParserW1 = (isLeft $ parseOnly stmListParser "(begin )")
testStmListParserW2 = (isLeft $ parseOnly stmListParser "(begin)")
testStmListParserW3 = (isLeft $ parseOnly stmListParser "(begi n skip)")

--skipParser
--right statement
testSkipParserR1 = (parseOnly skipParser "skip" == Right expectedSkip)
testSkipParserR2 = (parseOnly skipParser "  skip" == Right expectedSkip)
--wrong statement
testSkipParserW1 = (isLeft $ parseOnly skipParser "kskip")
testSkipParserW2 = (isLeft $ parseOnly skipParser "ski p")
testSkipParserW3 = (isLeft $ parseOnly skipParser "skiip")

--ifStmParser
--right statement
testIfStmParserR = (parseOnly ifStmParser "(if 1 (set! temp True) (set! temp False))" == Right expectedIfStm)
--wrong statement
testIfStmParserW = (isLeft $ parseOnly ifStmParser "(if 1 (set! temp True)")

--whileStmParser
--right statement
testWhileStmParserR1 = (parseOnly whileStmParser "(while True (+ 1 2))" == Right expectedWhileStm1)
testWhileStmParserR2 = (parseOnly whileStmParser "(while False (- 10 9))" == Right expectedWhileStm2)
--wrong statement
testWhileStmParserW1 = (isLeft $ parseOnly whileStmParser "(whil True (+ 1 2))")
testWhileStmParserW2 = (isLeft $ parseOnly whileStmParser "(whil (+ 1 2))")
testWhileStmParserW3 = (isLeft $ parseOnly whileStmParser "(whil True)")

--returnStmParser
--right statement
testReturnStmParserR1 = (parseOnly returnStmParser "(return 1)" == Right expectedReturnStm1)
testReturnStmParserR2 = (parseOnly returnStmParser "(   return   1)" == Right expectedReturnStm1)
testReturnStmParserR3 = (parseOnly returnStmParser "(return (+ 1 2))" == Right expectedReturnStm2)
--wrong statement
testReturnStmParserW1 = (isLeft $ parseOnly returnStmParser "(return)")
testReturnStmParserW2 = (isLeft $ parseOnly returnStmParser "(retu rn)")
testReturnStmParserW3 = (isLeft $ parseOnly returnStmParser "(retur)")
testReturnStmParserW4 = (isLeft $ parseOnly returnStmParser "(return 1 2)")

--setVarParser
--right expression
testSetVarExprR1 = (parseOnly setVarParser "(set! exampleVar 1)" == Right expectedSetVarExpr1)
testSetVarExprR2 = (parseOnly setVarParser "(  set! exampleVar 1)" == Right expectedSetVarExpr1)
testSetVarExprR3 = (parseOnly setVarParser "(set! exampleVar True)" == Right expectedSetVarExpr2)
--wrong expression
testSetVarExprW1 = (isLeft $ parseOnly setVarParser "(set exampleVar 1)")
testSetVarExprW2 = (isLeft $ parseOnly setVarParser "(set exampleVar 1 2)")

--makeVecParser
--right expression
testMakeVecExprR = (parseOnly makeVecParser "(make-vector exampleVec 10)" == Right expectedMakeVecExpr)
--wrong expression
testMakeVecExprW = (isLeft $ parseOnly makeVecParser "kskip")

--vecSetParser
--right expression
testVecSetExprR1 = (parseOnly vecSetParser "(vector-set! exampleVec 2 (+ 1 2))" == Right expectedVecSetExpr1)
testVecSetExprR2 = (parseOnly vecSetParser "(vector-set! exampleVec (+ 3 4) 4)" == Right expectedVecSetExpr2)
--wrong expression
testVecSetExprW1 = (isLeft $ parseOnly vecSetParser "(vector-set exampleVec 2 (+ 1 2))")
testVecSetExprW2 = (isLeft $ parseOnly vecSetParser "(vectorset exampleVec 2 (+ 1 2))")
testVecSetExprW3 = (isLeft $ parseOnly vecSetParser "(vector-set! exampleVec )")
testVecSetExprW4 = (isLeft $ parseOnly vecSetParser "(vector-set! exampleVec 2 )")

--stmExprParser
--right statement
testStmExprParserR = (parseOnly stmExprParser "1" == Right expectedStmExpr)

--statementListParser
--right statement
testStatementListParserR = (parseOnly statementListParser "skip (if True skip skip) (while True skip) (return 1) (set! exampleVar 1) (make-vector exampleVec 10) (vector-set! exampleVec 1 1) (define (exampleFunc arg1) skip)" 
    == Right expectedStatementListStm)
--wrong statement
testStatementListParserW = (isLeft $ parseOnly statementListParser "kskip")

--numberLiteralParser
--right expression
testNumberLiteralExprR = (parseOnly numberLiteralParser "1" == Right expectedNumberLiteralExpr)
--wrong expression
testNumberLiteralExprW1 = (isLeft $ parseOnly numberLiteralParser "a")
testNumberLiteralExprW2 = (isLeft $ parseOnly numberLiteralParser "'a'")
testNumberLiteralExprW3 = (isLeft $ parseOnly numberLiteralParser "asd132")

--charLiteralParser
--right expression
testCharLiteralExprR = (parseOnly charLiteralParser "\'a\'" == Right expectedCharLiteralExpr)
--wrong expression
testCharLiteralExprW1 = (isLeft $ parseOnly charLiteralParser "''a'")
testCharLiteralExprW2 = (isLeft $ parseOnly charLiteralParser "\'ab\'")
testCharLiteralExprW3 = (isLeft $ parseOnly charLiteralParser "\\'a'")
testCharLiteralExprW4 = (isLeft $ parseOnly charLiteralParser "\"ab\"")

--stringLiteralParser
--right expression
testStringLiteralExprR = (parseOnly stringLiteralParser "\"foo\"" == Right expectedStringLiteralExpr)
--wrong expression
testStringLiteralExprW1 = (isLeft $ parseOnly stringLiteralParser "foo\"")
testStringLiteralExprW2 = (isLeft $ parseOnly stringLiteralParser "\"foo")
testStringLiteralExprW3 = (isLeft $ parseOnly stringLiteralParser "fo\"o\"")

--boolLiteralParser
--right expression
testBoolLiteralExprR1 = (parseOnly boolLiteralParser "True" == Right expectedBoolLiteralExpr1)
testBoolLiteralExprR2 = (parseOnly boolLiteralParser "False" == Right expectedBoolLiteralExpr2)
--wrong expression
testBoolLiteralExprW1 = (isLeft $ parseOnly boolLiteralParser "ATrue")
testBoolLiteralExprW2 = (isLeft $ parseOnly boolLiteralParser "true")
testBoolLiteralExprW3 = (isLeft $ parseOnly boolLiteralParser "TRUE")
testBoolLiteralExprW4 = (isLeft $ parseOnly boolLiteralParser "Tr ue")

--nilLiteralParser
--right expression
testNilLiteralExprR1 = (parseOnly nilLiteralParser "nil" == Right expectedNilLiteralExpr)
testNilLiteralExprR2 = (parseOnly nilLiteralParser "  nil" == Right expectedNilLiteralExpr)
testNilLiteralExprR3 = (parseOnly nilLiteralParser "nil  " == Right expectedNilLiteralExpr)
--wrong expression
testNilLiteralExprW1 = (isLeft $ parseOnly nilLiteralParser "niil")
testNilLiteralExprW2 = (isLeft $ parseOnly nilLiteralParser "ni l")

--variableValueParser
--right expression
testVariableValueExprR = (parseOnly variableValueParser "val " == Right expectedVariableValueExpr)

--functionCallParser
--right expression
testFunctionCallExprR1 = (parseOnly functionCallParser "(+ 1 2)" == Right expectedFunctionCallExpr1)
testFunctionCallExprR2 = (parseOnly functionCallParser "(voidFunc)" == Right expectedFunctionCallExpr2)
--wrong expression
testFunctionCallExprW1 = (isLeft $ parseOnly functionCallParser "(+ 1 2")
testFunctionCallExprW2 = (isLeft $ parseOnly functionCallParser "+ 1 2)")
testFunctionCallExprW3 = (isLeft $ parseOnly functionCallParser "()")

--lambdaDefParser
--right expression
testLambdaDefExprR = (parseOnly lambdaDefParser "(lambda arg 1)" == Right expectedLambdaDefExpr)
--wrong expression
testLambdaDefExprW1 = (isLeft $ parseOnly lambdaDefParser "(lam2dba arg1 1)")
testLambdaDefExprW2 = (isLeft $ parseOnly lambdaDefParser "(lambda 1)")
testLambdaDefExprW3 = (isLeft $ parseOnly lambdaDefParser "(lambda)")
testLambdaDefExprW4 = (isLeft $ parseOnly lambdaDefParser "(lambda arg1 arg2 2)")

--functionDefParser
--right expression
testFunctionDefStmR1 = (parseOnly functionDefParser "(define (example arg1 arg2) skip)" == Right expectedFunctionDefStm1)
testFunctionDefStmR2 = (parseOnly functionDefParser "(define (voidFunc) skip)" == Right expectedFunctionDefStm2)
--wrong expression
testFunctionDefStmW1 = (isLeft $ parseOnly functionDefParser "(de fine (example arg2 2)")
testFunctionDefStmW2 = (isLeft $ parseOnly functionDefParser "(define example arg1 arg2 skip)")
testFunctionDefStmW3 = (isLeft $ parseOnly functionDefParser "(define skip)")
testFunctionDefStmW4 = (isLeft $ parseOnly functionDefParser "(define example skip)")

--test lexeme
--generate stmlist parser without lexeme
stmListParserWithoutLexeme :: Parser Statement
stmListParserWithoutLexeme = do
    char '('
    string "begin"
    list <- statementListParser
    char ')'
    return (StmList list)
-- hasSpace = parseOnly 
stmParserWithSpace = parseOnly stmListParser "(begin 233)"
stmParserWithoutSpace = parseOnly stmListParserWithoutLexeme "(begin 233)"
testLexeme = (stmParserWithSpace == stmParserWithoutSpace)

--identifierParser
--right
testIdentifierParserR1 = (parseOnly identifierParser "id1 " == Right expectedIdentifier1)
testIdentifierParserR2 = (parseOnly identifierParser " id2 " == Right expectedIdentifier2)
--wrong
testIdentifierParserW1 = (parseOnly identifierParser "i d1" == Right expectedIdentifier3)
testIdentifierParserW2 = (parseOnly identifierParser "(id1)" == Right expectedIdentifier4)

--invalidCharParser 
--right
testInvalidCharR1 = (parseOnly invalidCharParser " " == Right expectedInvalidChar1)
testInvalidCharR2 = (parseOnly invalidCharParser "(" == Right expectedInvalidChar2)
testInvalidCharR3 = (parseOnly invalidCharParser ")" == Right expectedInvalidChar3)
testInvalidCharR4 = (parseOnly invalidCharParser "," == Right expectedInvalidChar4)
testInvalidCharR5 = (parseOnly invalidCharParser "(abc" == Right expectedInvalidChar2)
--wrong
testInvalidCharW1 = (isLeft $ parseOnly invalidCharParser "a")
testInvalidCharW2 = (isLeft $ parseOnly invalidCharParser "1")

