{-# LANGUAGE OverloadedStrings #-}
module MLispTest.TestStringToVM where

import Test.QuickCheck
import MLispTest.TestCase

import Data.Map as Map (Map, fromList, member, (!))
import Data.Maybe (fromMaybe)
import Data.ByteString as BS (ByteString, pack, empty, length, append)
import System.IO (BufferMode(..), hSetBuffering, hFlush, stdout, stdin)
import System.Exit (exitSuccess)
import Control.Monad.Trans (lift)
import Control.Monad.State (StateT, get, modify, runStateT)

import MLisp.VM (
    MLispVM(..),
    MLispScope,
    MLispOp,
    makeVM,
    runVM,
    runOp,
    printObj,
    getReg,
    runIO)
import MLisp.Parser (Statement(..), parseProgram)
import MLisp.Compiler (MLispTransOp, evalTransOp, genByteCode, getEntryPoint,  makeTransCtx, transformAST, genByteCode, getCode, setOffset)


testStringToVMEntry :: IO ()
testStringToVMEntry = do
    skipStmString1Res <- testProgram skipStmString1
    quickCheck skipStmString1Res
    skipStmString2Res <- testProgram skipStmString2
    quickCheck skipStmString2Res
    putStrLn "skipStmString finished"
    ifStmStringRes <- testProgram ifStmString
    quickCheck ifStmStringRes
    putStrLn "ifStmString finished"
    whileStmString1Res <- testProgram whileStmString1
    quickCheck whileStmString1Res
    whileStmString2Res <- testProgram whileStmString2
    quickCheck whileStmString2Res
    putStrLn "whileStmString finished"
    returnStmStringRes <- testProgram returnStmString
    quickCheck returnStmStringRes
    putStrLn "returnStmString finished"
    setVarStmStringRes <- testProgram setVarStmString
    quickCheck setVarStmStringRes
    putStrLn "setVarStmString finished"
    makeVecStmStringRes <- testProgram makeVecStmString
    quickCheck makeVecStmStringRes
    putStrLn "makeVecStmString finished"
    vecSetStmString1Res <- testProgram vecSetStmString1
    quickCheck vecSetStmString1Res
    vecSetStmString2Res <- testProgram vecSetStmString2
    quickCheck vecSetStmString2Res
    putStrLn "vecSetStmString finished"
    stmListStmStringRes <- testProgram stmListStmString
    quickCheck stmListStmStringRes
    putStrLn "stmListStmString finished"
    numberExprStringRes <- testProgram numberExprString
    quickCheck numberExprStringRes
    putStrLn "numberExprString finished"
    charExprStringRes <- testProgram charExprString
    quickCheck charExprStringRes
    putStrLn "charExprString finished"
    boolExprString1Res <- testProgram boolExprString1
    quickCheck boolExprString1Res
    boolExprString2Res <- testProgram boolExprString2
    quickCheck boolExprString2Res
    putStrLn "boolExprString finished"
    nilExprStringRes <- testProgram nilExprString
    quickCheck nilExprStringRes
    putStrLn "nilExprString finished"
    variableValueExprStringRes <- testProgram variableValueExprString
    quickCheck variableValueExprStringRes
    putStrLn "variableValueExprString finished"
    functionCallString1Res <- testProgram functionCallString1
    quickCheck functionCallString1Res
    functionCallString2Res <- testProgram functionCallString2
    quickCheck functionCallString2Res
    putStrLn "functionCallString finished"
    lambdaDefExprStringRes <- testProgram lambdaDefExprString
    quickCheck lambdaDefExprStringRes
    putStrLn "lambdaDefExprString finished"
    functionDefStmString1Res <- testProgram functionDefStmString1
    quickCheck functionDefStmString1Res
    functionDefStmString2Res <- testProgram functionDefStmString2
    quickCheck functionDefStmString2Res
    putStrLn "functionDefStmString finished"

testTranslate_STV :: Statement -> MLispTransOp (ByteString, Int)
testTranslate_STV ast = do
    transformAST ast False
    -- Return code and entry point
    byteCode <- genByteCode
    entryPoint <- getEntryPoint
    return (byteCode, entryPoint)

testREPLVMOp :: Statement -> MLispOp ()
testREPLVMOp ast = do
    -- Run VM
    runVM
    -- Print value for expression
    case ast of
        (StmExpr _) -> do
            exprVal <- getReg 0
            printObj exprVal
            runIO $ putStrLn ""
        _ -> return ()

testProgram :: String -> IO Bool
testProgram input = 
    case parseProgram input of
        (Left _) -> return False
        (Right ast) -> case evalTransOp (testTranslate_STV ast) makeTransCtx of
            (Left _) -> return False
            (Right (byteCode, entryPoint)) -> do
                -- Build VM state
                let vm = makeVM {
                    program = byteCode,
                    counter = entryPoint
                }
                -- Run VM and print value for expression
                (result, newVM) <- runOp (testREPLVMOp ast) vm
                -- Prompt runtime errorzzaasasas
                case result of
                    (Left err) -> return False
                    _ -> return True
