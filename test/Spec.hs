import MLispTest.Parser.TestParser (testParserEntry)
import MLispTest.Compiler.TestCompiler (testCompilerEntry)
import MLispTest.VM.TestVM (testVMEntry)
import MLispTest.TestUtil (testUtilEntry)
import MLispTest.TestStringToVM (testStringToVMEntry)

main :: IO ()
main = do
    putStrLn ""
    putStrLn "---------------------TEST PARSER START---------------------"
    testParserEntry
    putStrLn "---------------------TEST PARSER FINISHED---------------------"
    putStrLn ""
    putStrLn ""
    putStrLn ""
    putStrLn "---------------------TEST COMPILER START---------------------"
    testCompilerEntry
    putStrLn "---------------------TEST COMPILER FINISHED---------------------"
    putStrLn ""
    putStrLn ""
    putStrLn ""
    putStrLn "---------------------TEST VM START---------------------"
    testVMEntry
    putStrLn "---------------------TEST VM FINISHED---------------------"
    putStrLn ""
    putStrLn ""
    putStrLn ""
    putStrLn "---------------------TEST UTIL START---------------------"
    testUtilEntry
    putStrLn "---------------------TEST UTIL FINISHED---------------------"
    putStrLn ""
    putStrLn ""
    putStrLn ""
    putStrLn "---------------------TEST STRING_TO_VM START---------------------"
    testStringToVMEntry
    putStrLn "---------------------TEST STRING_TO_VM FINISHED---------------------"
    