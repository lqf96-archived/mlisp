module MLisp.VM (
        module MLisp.VM.Types,
        module MLisp.VM.VM,
        module MLisp.VM.Object,
        makeVM,
        runVM,
        runOp,
        printObj
    ) where

import Data.ByteString as BS (ByteString, empty)
import Data.Map as Map (empty, member, (!))
import Data.Vector as Vector (replicate)
import Control.Monad.Except (runExceptT)
import Control.Monad.State (runStateT)

import MLisp.Util (guard)
import MLisp.VM.Types
import MLisp.VM.VM hiding (getter, setter)
import MLisp.VM.Object
import MLisp.VM.Instructions (haltOpcode, fetchByte, opcodeInstMapping)
import MLisp.VM.Builtin (injectHaskellFuncs, injectHffiFuncs, printObj)

-- Make VM
makeVM :: MLispVM
makeVM = MLispVM {
    -- Registers
    registers = Vector.replicate nRegisters $ makeObj (),
    -- Global variables
    globals = injectHaskellFuncs $ injectHffiFuncs $ Map.empty,
    -- Local variables
    locals = Nothing,
    -- Program data
    program = BS.empty,
    -- Program counter
    counter = 0,
    -- Call stack
    callStack = [],
    -- Output writer
    outputWriter = putStr
}

-- Run VM
runVM :: MLispOp ()
runVM = do
    op <- fetchByte
    -- Halted
    if op == haltOpcode
        then return ()
        else do
            -- Unknown opcode; raise error
            guard (member op opcodeInstMapping) UnknownOpcode { opcode = op }
            -- Execute current instruction
            opcodeInstMapping ! op
            -- Run next instruction
            runVM

-- Run MLisp operations
runOp :: MLispOp a -> MLispVM -> IO (Either MLispRuntimeError a, MLispVM)
runOp op vm = runStateT (runExceptT op) vm
