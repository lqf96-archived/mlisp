module MLisp.Compiler.Context where

import Data.Map (Map, insert, (!))
import Data.Vector (Vector, snoc, empty)
import Control.Monad.Except (throwError)

import MLisp.Util (getter, setter, getState, setState, guard, intVLen)
import MLisp.Compiler.Types (MLispFuncCtx(..), MLispTransCtx(..), MLispInst(..), MLispLabel, MLispTransOp)

-- [ Utilities ]
-- Get transform context
getCtx :: MLispTransOp MLispTransCtx
getCtx = getState

-- Set transform context
setCtx :: MLispTransCtx -> MLispTransOp ()
setCtx = setState

-- Function context getter
funcCtxGetter :: (MLispFuncCtx -> a) -> MLispTransOp a
funcCtxGetter f = getter (f . funcCtx)

-- Function context setter
funcCtxSetter :: (MLispFuncCtx -> MLispFuncCtx) -> MLispTransOp ()
funcCtxSetter f = setter (\vm ->
    let newFCtx = f $ funcCtx vm
    in vm { funcCtx = newFCtx })

-- [ Instructions ]
-- Length of instructions
instLen :: MLispInst -> Int
instLen inst = case inst of
    -- Movement instructions
    Rrmov {} -> 3
    Rmmov {} -> 3
    Mrmov {} -> 3
    -- Immediate movement instructions
    Irmovn {} -> 2
    Irmovi { intVal = num } -> intVLen num + 3
    Irmovb {} -> 3
    Irmovc {} -> 3
    Irmovs { strVal = bytes } -> length bytes + 6
    Irmovr { intVal1 = num1, intVal2 = num2 } -> intVLen num1 + intVLen num2 + 4
    -- Subroutine instructions
    Call {} -> 3
    Ret {} -> 2
    -- Jump instructions (With intermediate form)
    Jmp {} -> 5
    Ijmp {} -> 5
    Cj {} -> 6
    Icj {} -> 6
    -- High-order function instructions (With intermediate form)
    Mkfunc {} -> 7
    Imkfunc {} -> 7
    -- Halt
    Halt {} -> 1
    -- Label (Intermediate only)
    Label {} -> 0

-- [ Transform Context ]
-- Get function context
getFuncCtx :: MLispTransOp MLispFuncCtx
getFuncCtx = getter funcCtx

-- Set function context
setFuncCtx :: MLispFuncCtx -> MLispTransOp ()
setFuncCtx newFuncCtx = setter (\ctx -> ctx { funcCtx = newFuncCtx })

-- Get next function label
getNextFuncLabel :: MLispTransOp MLispLabel
getNextFuncLabel = getter (\ctx -> (nextFuncId ctx, 0))

-- Save function context stack
saveFuncStack :: MLispTransOp ()
saveFuncStack = setter (\ctx ->
    let fId = nextFuncId ctx
        newFuncId = fId + 1
        newFuncCtx = MLispFuncCtx {
            regStackTop = 0,
            funcCode = empty,
            funcId = fId,
            nextLocalId = 1
        }
        newFuncStack = funcCtx ctx : funcStack ctx
    in ctx { funcCtx = newFuncCtx, funcStack = newFuncStack, nextFuncId = newFuncId })

-- Get instructions
getCode :: MLispTransOp (Vector MLispInst)
getCode = getter code

-- Add code to function context
addToFuncCtx :: MLispInst -> MLispTransOp ()
addToFuncCtx inst = case inst of
    -- Resolve label position and add label-offset mapping
    (Label { label = l }) -> setter (\ctx ->
        let newMapping = insert l (codeLen ctx) $ labelOffsetMapping ctx
        in ctx { labelOffsetMapping = newMapping })
    -- Append other label
    _ -> setter (\ctx ->
        let newCodeLen = codeLen ctx + instLen inst
            newCode = snoc (code ctx) inst
        in ctx { code = newCode, codeLen = newCodeLen })

-- Pop function context stack
popFuncStack :: MLispTransOp ()
popFuncStack = do
    ctx <- getCtx
    -- Cannot pop from empty stack
    guard (length (funcStack ctx) > 0) "empty stack, cannot pop"
    -- Pop stack top function context
    let (topFCtx:remain) = funcStack ctx
    let newCtx = ctx { funcCtx = topFCtx, funcStack = remain }
    -- Set new context
    setCtx newCtx
    -- Append current function code to context code area
    -- Record label-offset mapping on-the-fly
    mapM_ addToFuncCtx $ funcCode $ funcCtx ctx

-- Get entry point address
getEntryPoint :: MLispTransOp Int
getEntryPoint = getter (\ctx -> labelOffsetMapping ctx ! (0, 0))

-- Set offset
setOffset :: Int -> MLispTransOp ()
setOffset newOffset = setter (\ctx -> ctx { codeLen = newOffset })

-- [ Function Context ]
-- Reserve registers
useRegs :: Int -> MLispTransOp Int
useRegs n = do
    fCtx <- getFuncCtx
    -- Get and increase register stack top
    let top = regStackTop fCtx
    let newFCtx = fCtx { regStackTop = top + n }
    -- Set new function context and return old stack top
    setFuncCtx newFCtx
    return top

-- Pop registers
popRegs :: Int -> MLispTransOp ()
popRegs n = funcCtxSetter (\fCtx ->
    let newStackTop = regStackTop fCtx - n
    in fCtx { regStackTop = newStackTop })

-- Append instructions
appendInst :: MLispInst -> MLispTransOp ()
appendInst inst = funcCtxSetter (\fCtx ->
    let newFuncCode = snoc (funcCode fCtx) inst
    in fCtx { funcCode = newFuncCode })

-- Make label
makeLabel :: MLispTransOp MLispLabel
makeLabel = do
    fCtx <- getFuncCtx
    -- Get and increase local label ID
    let localId = nextLocalId fCtx
    let newFCtx = fCtx { nextLocalId = localId + 1 }
    -- Set new function context and return label
    setFuncCtx newFCtx
    return (funcId fCtx, localId)
