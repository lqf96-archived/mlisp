module MLisp.Compiler.Bytecode where

import Data.ByteString as BS (ByteString, pack, concat)
import Data.Word (Word8)
import Data.Char (ord)
import Data.Vector (toList)

import MLisp.Util (intVLen, makeIntV, makeInt32)
import MLisp.Compiler.Types (MLispTransCtx(..), MLispInst(..), MLispTransOp)
import MLisp.Compiler.Context (getCode)

-- Encode instruction
encodeInst :: MLispInst -> [Int]
encodeInst inst = case inst of
    -- Movement instructions
    Rrmov { fromReg = byte1, toReg = byte2 } -> [0x0, byte1, byte2]
    Rmmov { fromReg = byte1, toVarReg = byte2 } -> [0x10, byte1, byte2]
    Mrmov { fromVarReg = byte1, toReg = byte2 } -> [0x20, byte1, byte2]
    -- Immediate movement instructions
    Irmovn { toReg = byte1 } -> [0x30, byte1]
    Irmovi { intVal = intVI, toReg = byte1 } -> [0x31] ++ makeIntV intVI ++ [byte1]
    Irmovc { charVal = charI, toReg = byte1 } -> [0x32, ord charI, byte1]
    Irmovs { strVal = strI, toReg = byte1 } ->
        let strLenData = makeInt32 $ length strI
        in [0x33] ++ strLenData ++ fmap ord strI ++ [byte1]
    Irmovb { boolVal = boolI, toReg = byte2 } ->
        let byte1 = if boolI then 1 else 0
        in [0x34, byte1, byte2]
    Irmovr { intVal1 = intVI1, intVal2 = intVI2, toReg = byte1 } ->
        [0x35] ++ makeIntV intVI1 ++ makeIntV intVI2 ++ [byte1]
    -- Subroutine instructions
    Call { nArgs = byte1, baseReg = byte2 } -> [0x40, byte1, byte2]
    Ret { retValReg = byte1 } -> [0x50, byte1]
    -- Jump instructions
    Jmp { addr = intD1 } -> [0x60] ++ makeInt32 intD1
    Cj { condReg = byte1, addr = intD1 } -> [0x70, byte1] ++ makeInt32 intD1
    -- High-order function instructions
    Mkfunc { nArgs = byte1, addr = intD1, toReg = byte2 } -> [0x80, byte1] ++ makeInt32 intD1 ++ [byte2]
    -- Halt
    Halt -> [0x90]

-- Generate bytecode
genByteCode :: MLispTransOp ByteString
genByteCode = do
    instructions <- getCode
    -- Encode instructions
    let encoder = pack . (fmap fromIntegral) . encodeInst
    let code = fmap encoder $ toList instructions
    -- Concatenate bytecode strings
    return $ BS.concat code
