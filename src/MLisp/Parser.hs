module MLisp.Parser (
        module MLisp.Parser.Parser,
        module MLisp.Parser.AST,
        module MLisp.Parser.Util,
        parseProgram
    ) where

import Data.Attoparsec.Text (parseOnly)
import Data.Text (pack)

import MLisp.Parser.Parser
import MLisp.Parser.AST
import MLisp.Parser.Util

-- Parse program
parseProgram :: String -> Either String Statement
parseProgram input = parseOnly statementParser $ pack input
