module MLisp.Parser.AST where

type Variable = [Char]

-- Basic statement
data Statement
    = StmList [Statement]
    | Skip
    | IfStm { iCond :: Expression, trueStm :: Statement, falseStm :: Statement}
    | WhileStm { wCond :: Expression, stm :: Statement }
    | ReturnStm { value :: Expression }
    | SetVar { var :: Variable, value :: Expression }
    | MakeVec { var :: Variable, len :: Expression}
    | VecSet { var :: Variable, index :: Expression, value :: Expression }
    | StmExpr Expression
    | FunctionDef { name :: Variable, argList :: [Variable], statements :: Statement }
    deriving (Eq)

-- basic expression
data Expression
    = NumberLiteral Rational
    | CharLiteral Char
    | StringLiteral String
    | BoolLiteral Bool
    | NilLiteral
    | VariableValue Variable
    | FunctionCall Expression [Expression]
    | LambdaDef Variable Expression
    deriving (Eq)
