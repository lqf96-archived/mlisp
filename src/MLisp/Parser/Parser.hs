{-# LANGUAGE OverloadedStrings #-}

module MLisp.Parser.Parser where

import MLisp.Parser.AST
import Control.Applicative
import Control.Monad (when)
import Data.Functor
import Data.Text
import Data.Attoparsec.Text
import Data.Attoparsec.Combinator (lookAhead)

-- parser for Statement
statementParser :: Parser Statement
statementParser = stmListParser <|> skipParser <|> ifStmParser <|> whileStmParser <|> returnStmParser <|> setVarParser <|> makeVecParser <|> vecSetParser <|> functionDefParser <|> stmExprParser

expressionParser :: Parser Expression
expressionParser = numberLiteralParser <|> charLiteralParser <|> stringLiteralParser <|> boolLiteralParser <|> nilLiteralParser <|> lambdaDefParser <|> functionCallParser <|> variableValueParser

-- parser for (StmList StatementList)
stmListParser :: Parser Statement
stmListParser = do
    lexeme $ char '('
    lexeme $ string "begin"
    list <- lexeme $ statementListParser
    lexeme $ char ')'
    return (StmList list)

-- parser for (Skip)
skipParser :: Parser Statement
skipParser = do
    lexeme $ string "skip"
    return Skip

-- parser for (IfStm Expression Statement Statement)
ifStmParser :: Parser Statement
ifStmParser = do
    lexeme $ char '('
    lexeme $ string "if"
    iCond <- lexeme $ expressionParser
    trueStm <- lexeme $ statementParser
    falseStm <- lexeme $ statementParser
    lexeme $ char ')'
    return (IfStm iCond trueStm falseStm)

-- parser for (While Expression Statement)
whileStmParser :: Parser Statement
whileStmParser = do
    lexeme $ char '('
    lexeme $ string "while"
    wCond <- lexeme $ expressionParser
    stm <- lexeme $ statementParser
    lexeme $ char ')'
    return (WhileStm wCond stm)

-- parser for (ReturnStm Expression)
returnStmParser :: Parser Statement
returnStmParser = do
    lexeme $ char '('
    lexeme $ string "return"
    value <- lexeme $ expressionParser
    lexeme $ char ')'
    return (ReturnStm value)

-- parser for (SetVar Variable Expression)
setVarParser :: Parser Statement
setVarParser = do
    lexeme $ char '('
    lexeme $ string "set!"
    --skipSpace
    var <- lexeme $ identifierParser
    value <- lexeme $ expressionParser
    lexeme $ char ')'
    return (SetVar var value)

-- parser for (MakeVec Variable Expression)
makeVecParser :: Parser Statement
makeVecParser = do
    lexeme $ char '('
    lexeme $ string "make-vector"
    var <- lexeme $ identifierParser
    len <- lexeme $ expressionParser
    lexeme $ char ')'
    return (MakeVec var len)

-- parser for (VecSet var index value)
vecSetParser :: Parser Statement
vecSetParser = do
    lexeme $ char '('
    lexeme $ string "vector-set!"
    var <- lexeme $ identifierParser
    index <- lexeme $ expressionParser
    value <- lexeme $ expressionParser
    lexeme $ char ')'
    return (VecSet var index value)

-- parser for (StmExpr Expression)
stmExprParser :: Parser Statement
stmExprParser = do
    expr <- lexeme $ expressionParser
    return (StmExpr expr)

-- parser for (StatementList [Statement])
statementListParser :: Parser [Statement]
statementListParser = do
    list <- statementParser `delimittedBy` (char ' ')
    return list

numberLiteralParser :: Parser Expression
numberLiteralParser = do
    val <- lexeme $ rational
    return (NumberLiteral val)

charLiteralParser :: Parser Expression
charLiteralParser = do
    lexeme $ char '\''
    val <- anyChar
    char '\''
    return (CharLiteral val)

stringLiteralParser :: Parser Expression
stringLiteralParser = do
    lexeme $ char '\"'
    val <- manyUntil anyChar (char '\"')
    char '\"'
    return (StringLiteral val)

boolLiteralParser :: Parser Expression
boolLiteralParser = do
    skipWhiteSpace
    (string "True" $> BoolLiteral True) <|> (string "False" $> BoolLiteral False)

nilLiteralParser :: Parser Expression
nilLiteralParser = lexeme $ string "nil" $> NilLiteral

variableValueParser :: Parser Expression
variableValueParser = do
    --skipSpace
    var <- lexeme $ identifierParser
    return (VariableValue var)

functionCallParser :: Parser Expression
functionCallParser = do
    lexeme $ char '('
    skipWhiteSpace
    (func:args) <- expressionParser `delimittedBy` (char ' ')
    lexeme $ char ')'
    return (FunctionCall func args)


lambdaDefParser :: Parser Expression
lambdaDefParser = do
    lexeme $ char '('
    lexeme $ string "lambda"
    var <- lexeme $ identifierParser
    index <- lexeme $ expressionParser
    lexeme $ char ')'
    return (LambdaDef var index)

functionDefParser :: Parser Statement
functionDefParser = do
    lexeme $ char '('
    lexeme $ string "define"
    lexeme $ char '('
    --skipSpace
    (name:argList) <- identifierParser `delimittedBy` (char ' ')
    lexeme $ char ')'
    stm <- lexeme $ statementParser
    lexeme $ char ')'
    return (FunctionDef name argList stm)

skipWhiteSpace :: Parser ()
skipWhiteSpace = skipWhile (flip elem [' ', '\n', '\t'])

lexeme :: Parser a -> Parser a
lexeme p = do
    skipWhiteSpace
    p

identifierParser :: Parser [Char]
identifierParser = do
    skipSpace
    var <- manyUntil anyChar invalidCharParser
    return var

invalidCharParser :: Parser Char
invalidCharParser =
    (char ' ') <|>
    (char '(') <|>
    (char ')') <|>
    (char ',') <|>
    (char '\n') <|>
    (char '\t')

manyUntil :: Parser a -> Parser b -> Parser [a]
manyUntil p deli = do
    first <- p
    next <- manyUntil' p deli
    return (first:next)

manyUntil' :: Parser a -> Parser b -> Parser [a]
manyUntil' p deli = do
    eof <- atEnd
    if eof 
        then return []
        else (lookAhead deli >> return []) <|> (manyUntil p deli)

delimittedBy :: Parser a -> Parser b -> Parser [a]
delimittedBy p deli = do
    first <- p
    next <- delimittedBy' p deli
    return (first:next)

delimittedBy' :: Parser a -> Parser b -> Parser [a]
delimittedBy' p deli =
    -- (deli >> (delimittedBy p deli))
    (do {deli; skipWhiteSpace; c <- lookAhead anyChar; if c == ')' then return [] else delimittedBy p deli})
    <|> (return [])
