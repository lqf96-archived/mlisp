module MLisp.Parser.Util where

import MLisp.Parser.AST (Statement(..), Expression(..))
import Data.List (intercalate)

instance Show Statement where
    show = showStmt (-2) [0] True ""

instance Show Expression where
    show = showExpr (-2) [0] True ""

showStmt :: Int -> [Int] -> Bool -> String -> Statement -> String
showStmt ind l final prefix stmt = case stmt of
    (StmList list) -> indent ++ prefix ++ "StmList\n" ++ showStmtList list
    (Skip) -> indent ++ prefix ++ "Skip\n"
    (IfStm {iCond=condExpr, trueStm=trueStmt, falseStm=falseStmt}) -> indent ++ prefix ++ "IfStm\n" ++ nextExpr "iCond: " condExpr ++ nextStmt "trueStm: " trueStmt ++ finalStmt "falseStm: " falseStmt
    (WhileStm {wCond=condExpr, stm=stmt}) -> indent ++ prefix ++ "WhileStm\n" ++ nextExpr "wCond: " condExpr ++ finalStmt "stmts: " stmt
    (ReturnStm {value=expr}) -> indent ++ prefix ++ "RetrunStm\n" ++ finalExpr "retVal: " expr
    (SetVar {var=name, value=val}) -> indent ++ prefix ++ "Set Variable <name=" ++ name ++ ">\n" ++ finalExpr "value: " val
    (MakeVec {var=name, len=length}) -> indent ++ prefix ++ "VectorDef <name=" ++ name ++ ">\n" ++ finalExpr "length: " length
    (VecSet {var=name, index=expr, value=val}) -> indent ++ prefix ++ "VectorSet <name=" ++ name ++ ">\n" ++ nextExpr "index: " expr ++ finalExpr "value: " val 
    (StmExpr expr) -> indent ++ prefix ++ "Expression Statement\n" ++ finalExpr "" expr
    (FunctionDef {name=funcName, argList=list, statements=stmt}) -> indent ++ prefix ++ "FunctionDef <name=" ++ funcName ++ ", args=[" ++ (intercalate "," list) ++ "]>\n" ++ finalStmt "stmts: " stmt
    where indent = showIndent ind l final
          nextL = if final then l else (ind+2:l)
          nextExpr = showExpr (ind+2) nextL False
          finalExpr = showExpr (ind+2) nextL True
          nextStmt = showStmt (ind+2) nextL False
          finalStmt = showStmt (ind+2) nextL True
          showStmtList (x:[]) = finalStmt "" x
          showStmtList (x:xs) = nextStmt "" x ++ showStmtList xs

showExpr :: Int -> [Int] -> Bool -> String -> Expression -> String
showExpr ind l final prefix expr = case expr of
    (NumberLiteral val) -> indent ++ prefix ++ "NumberLiteral <val=" ++ show val ++ ">\n"
    (CharLiteral val) -> indent ++ prefix ++ "CharLiteral <val=" ++ show val ++ ">\n"
    (StringLiteral val) -> indent ++ prefix ++ "StringLiteral <val=" ++ show val ++ ">\n"
    (BoolLiteral val) -> indent ++ prefix ++ "BoolLiteral <val=" ++ show val ++ ">\n"
    (NilLiteral) -> indent ++ prefix ++ "NilLiteral\n"
    (VariableValue name) -> indent ++ prefix ++ "VariableValue <var=" ++ name ++ ">\n"
    (FunctionCall func exprs) -> indent ++ prefix ++ "FunctionCall\n" ++ nextExpr "function: " func ++ showExprList "arg: " exprs
    (LambdaDef var expr) -> indent ++ prefix ++ "LambdaDef <var=" ++ var ++ ">\n" ++ finalExpr "" expr
    where indent = showIndent ind l final
          nextL = if final then l else (ind+2:l)
          nextExpr = showExpr (ind+2) nextL False
          finalExpr = showExpr (ind+2) nextL True
          showExprList pref (x:[]) = finalExpr pref x
          showExprList pref (x:xs) = nextExpr pref x ++ showExprList pref xs

showIndent :: Int -> [Int] -> Bool -> String
showIndent (-2) _ _ = ""
showIndent ind l final = indent' ind l ++ if final then "`-" else "|-"

indent' :: Int -> [Int] -> String
indent' ind (0:xs) = take ind (repeat ' ')
indent' ind all@(x:xs) = if ind == x
    then (indent' (ind - 2) xs) ++ "| "
    else (indent' (ind - 2) all) ++ "  "