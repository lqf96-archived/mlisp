module MLisp.Compiler (
        module MLisp.Compiler.Types,
        module MLisp.Compiler.Transform,
        module MLisp.Compiler.Context,
        module MLisp.Compiler.Bytecode,
        makeTransCtx,
        transformAST,
        evalTransOp,
        execTransOp
    ) where

import Data.Vector as Vector (empty)
import Data.Map as Map (empty)
import Control.Monad.State (evalState, execState)
import Control.Monad.Except (runExceptT)

import MLisp.Parser.AST (Statement)
import MLisp.Compiler.Types
import MLisp.Compiler.Transform
import MLisp.Compiler.Context hiding (funcCtxGetter, funcCtxSetter, intVLen)
import MLisp.Compiler.Bytecode

-- Make transform context
makeTransCtx :: MLispTransCtx
makeTransCtx =
    -- Initial global "function" context
    let fCtx = MLispFuncCtx {
        regStackTop = 0,
        funcCode = Vector.empty,
        funcId = 0,
        nextLocalId = 1
    }
    -- Transform context
    in MLispTransCtx {
        funcCtx = fCtx,
        funcStack = [],
        labelOffsetMapping = Map.empty,
        code = Vector.empty,
        codeLen = 0,
        nextFuncId = 1
    }

-- Transform AST
transformAST :: Statement -> Bool -> MLispTransOp ()
transformAST stmts runMain = do
    -- AST transform phase
    astTransformPhase stmts runMain
    -- Resolve label phase
    resolveLabelPhase

-- Run transform operation
evalTransOp :: MLispTransOp a -> MLispTransCtx -> Either MLispCompileError a
evalTransOp op ctx = evalState (runExceptT op) ctx

execTransOp :: MLispTransOp a -> MLispTransCtx -> MLispTransCtx
execTransOp op ctx = execState (runExceptT op) ctx
