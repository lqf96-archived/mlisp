module MLisp.VM.VM where

import Data.Maybe (Maybe(..), isJust, fromJust, fromMaybe)
import Data.Vector as Vector (Vector, (!), (//))
import Data.Map as Map ((!), member, insert, empty)
import Data.ByteString as BS (ByteString, length)
import Control.Monad.Except (throwError)
import Control.Monad.Trans (lift)

import MLisp.Util (guard, getter, setter, getState, setState)
import MLisp.VM.Types (MLispVM(..), MLispStackFrame(..), MLispRuntimeError(..), MLispObj, MLispOp, MLispScope)

-- [ Constants ]
-- Register amount
nRegisters :: Int
nRegisters = 256

-- [ Utilties ]
-- Get VM
getVM :: MLispOp MLispVM
getVM = getState

-- Set VM
setVM :: MLispVM -> MLispOp ()
setVM = setState

-- Run IO operations
runIO :: IO a -> MLispOp a
runIO = lift . lift

-- [ Functions ]
-- Registers
getRegs :: MLispOp (Vector MLispObj)
getRegs = getter registers

getReg :: Int -> MLispOp MLispObj
getReg index = getter ((Vector.! index) . registers)

setRegs :: Vector MLispObj -> MLispOp ()
setRegs newRegs = setter (\vm -> vm { registers = newRegs })

setReg :: Int -> MLispObj -> MLispOp ()
setReg index value = setter (\vm ->
    let newRegs = registers vm // [(index, value)]
    in vm { registers = newRegs })

-- Global variables
getGlobals :: MLispOp MLispScope
getGlobals = getter globals

getGlobal :: String -> MLispOp MLispObj
getGlobal name = do
    vm <- getVM
    -- Assure variable exists in global scope
    guard (member name $ globals vm) VariableNotFound { varName = name }
    -- Return global variable
    return $ (globals vm) Map.! name

setGlobal :: String -> MLispObj -> MLispOp ()
setGlobal name value = setter (\vm ->
    let newGlobals = insert name value $ globals vm
    in vm { globals = newGlobals })

hasGlobal :: String -> MLispOp Bool
hasGlobal name = getter ((member name) . globals)

-- Local variables
getLocals :: MLispOp (Maybe MLispScope)
getLocals = getter locals

hasLocalScope :: MLispOp Bool
hasLocalScope = getter (isJust . locals)

getLocal :: String -> MLispOp MLispObj
getLocal name = do
    vm <- getVM
    -- Assure local scope exists
    guard (isJust $ locals vm) NoLocalScope
    let scope = fromJust $ locals vm
    -- Assure variable exists in local scope
    guard (member name scope) VariableNotFound { varName = name }
    -- Return local variable
    return $ scope Map.! name

setLocal :: String -> MLispObj -> MLispOp ()
setLocal name value = do
    vm <- getVM
    -- Assure local scope exists
    guard (isJust $ locals vm) NoLocalScope
    let scope = fromJust $ locals vm
    -- Update local variables and VM
    let newScope = insert name value scope
    setVM vm { locals = Just newScope }

hasLocal :: String -> MLispOp Bool
hasLocal name = getter ((member name) . (fromMaybe empty) . locals)

-- Variable getter & setter
getVar :: String -> MLispOp MLispObj
getVar name = do
    vm <- getVM
    -- Find in local scope first
    let localVars = locals vm
    if isJust localVars && member name (fromJust localVars)
        then return (fromJust localVars Map.! name)
        -- Then find in global scope
        else if member name $ globals vm
            then return (globals vm Map.! name)
            else throwError VariableNotFound { varName = name }

setVar :: String -> MLispObj -> MLispOp ()
setVar name value = do
    runInFunc <- hasLocalScope
    isGlobalVar <- hasGlobal name
    -- Set / create local scope variable if
    -- 1) Local scope exists
    -- 2) Variable not in global scope
    let setVarFunc = if runInFunc && not isGlobalVar then setLocal else setGlobal
    -- Set variable
    setVarFunc name value

hasVar :: String -> MLispOp Bool
hasVar name = do
    globalScope <- getGlobals
    maybeLocalScope <- getLocals
    let localScope = fromMaybe empty maybeLocalScope
    -- Lookup in local and global scope
    return (member name localScope || member name globalScope)

-- Program data
getProgram :: MLispOp ByteString
getProgram = getter program

setProgram :: ByteString -> MLispOp ()
setProgram newProgram = setter (\vm -> vm { program = newProgram })

-- Counter
getCounter :: MLispOp Int
getCounter = getter counter

setCounter :: Int -> MLispOp ()
setCounter newCounter = setter (\vm -> vm { counter = newCounter })

-- Stack frame
saveStackFrame :: Int -> MLispOp ()
saveStackFrame resultReg = setter (\vm ->
    let newStackFrame = MLispStackFrame {
            savedLocals = locals vm,
            savedRegs = registers vm,
            resultReg = resultReg,
            savedCounter = counter vm
        }
        newCallStack = newStackFrame : callStack vm
    in vm { callStack = newCallStack })

popStackFrame :: MLispOp Int
popStackFrame = do
    vm <- getVM
    case callStack vm of
        -- No stack frame in stack
        [] -> throwError EmptyCallStack
        -- Pop stack top frame
        (topFrame:remain) -> do
            -- Restore and update VM
            setVM vm {
                locals = savedLocals topFrame,
                registers = savedRegs topFrame,
                callStack = remain,
                counter = savedCounter topFrame
            }
            -- Return result register
            return $ resultReg topFrame

-- Output writer
getOutputWriter :: MLispOp (String -> IO ())
getOutputWriter = getter outputWriter
